'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _config = require('../config');

var _sequelize = require('../lib/sequelize');

var _initData = require('../initData');

var _initData2 = _interopRequireDefault(_initData);

var _promotion_type = require('./promotion_type.model');

var _promotion_type2 = _interopRequireDefault(_promotion_type);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PromotionListModel = _sequelize.db.define(_config.DB.prefix + 'promotion_list', {
  id: {
    type: _sequelize.Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  uuid: {
    type: _sequelize.Sequelize.STRING(36)
  },
  type_id: {
    type: _sequelize.Sequelize.STRING(4),
    unique: true
  },
  product_sku: {
    type: _sequelize.Sequelize.STRING(6)
  },
  variable1: {
    type: _sequelize.Sequelize.STRING(32)
  },
  variable2: {
    type: _sequelize.Sequelize.STRING(32)
  },
  variable3: {
    type: _sequelize.Sequelize.STRING(32)
  },
  variable4: {
    type: _sequelize.Sequelize.STRING(32)
  },
  start_time: {
    type: _sequelize.Sequelize.DATE
  },
  end_time: {
    type: _sequelize.Sequelize.DATE
  },
  createdAt: _sequelize.Sequelize.DATE,
  updatedAt: _sequelize.Sequelize.DATE
}, {
  tableName: _config.DB.prefix + 'promotion_list',
  comment: 'promotion list'
});

// create table if not existed
PromotionListModel.sync({
  create: true
}).then(function () {
  _initData2.default.promotionList.map(function (e) {
    PromotionListModel.findOrCreate({
      where: {
        uuid: e.uuid
      },
      defaults: _extends({}, e)
    });
  });
  PromotionListModel.belongsTo(_promotion_type2.default, { foreignKey: 'type_id', as: 'promotion_type' });
});

exports.default = PromotionListModel;
//# sourceMappingURL=promotion_list.model.js.map
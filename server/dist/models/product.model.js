'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _config = require('../config');

var _sequelize = require('../lib/sequelize');

var _initData = require('../initData');

var _initData2 = _interopRequireDefault(_initData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProductModel = _sequelize.db.define(_config.DB.prefix + 'products', {
  id: {
    type: _sequelize.Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  sku: {
    type: _sequelize.Sequelize.STRING(6)
  },
  name: {
    type: _sequelize.Sequelize.STRING(256)
  },
  price: {
    type: _sequelize.Sequelize.FLOAT(16)
  },
  currency: {
    type: _sequelize.Sequelize.STRING(8),
    defaultValue: 'USD'
  },
  inventory_qty: {
    type: _sequelize.Sequelize.INTEGER,
    defaultValue: 0
  },
  state: {
    type: _sequelize.Sequelize.INTEGER,
    defaultValue: 1 // -1 del, 0 offline, 1 online
  },
  promotion: {
    type: _sequelize.Sequelize.STRING(32)
  },
  createdAt: _sequelize.Sequelize.DATE,
  updatedAt: _sequelize.Sequelize.DATE
}, {
  tableName: _config.DB.prefix + 'products',
  comment: 'products list'
});

// create table if not existed
ProductModel.sync({
  create: true
}).then(function () {
  _initData2.default.productList.map(function (e) {
    ProductModel.findOrCreate({
      where: {
        sku: e.sku
      },
      defaults: _extends({}, e)
    });
  });
});

exports.default = ProductModel;
//# sourceMappingURL=product.model.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PromotionListModel = exports.PromotionTypeModel = exports.productModel = undefined;

var _product = require('./product.model');

var _product2 = _interopRequireDefault(_product);

var _promotion_type = require('./promotion_type.model');

var _promotion_type2 = _interopRequireDefault(_promotion_type);

var _promotion_list = require('./promotion_list.model');

var _promotion_list2 = _interopRequireDefault(_promotion_list);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.productModel = _product2.default;
exports.PromotionTypeModel = _promotion_type2.default;
exports.PromotionListModel = _promotion_list2.default;
//# sourceMappingURL=index.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _config = require('../config');

var _sequelize = require('../lib/sequelize');

var _initData = require('../initData');

var _initData2 = _interopRequireDefault(_initData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PromotionTypeModel = _sequelize.db.define(_config.DB.prefix + 'promotion_type', {
  id: {
    type: _sequelize.Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  type_id: {
    type: _sequelize.Sequelize.STRING(4),
    unique: true
  },
  name: {
    type: _sequelize.Sequelize.STRING(256)
  },
  createdAt: _sequelize.Sequelize.DATE,
  updatedAt: _sequelize.Sequelize.DATE
}, {
  tableName: _config.DB.prefix + 'promotion_type',
  comment: 'promotion type'
});

// create table if not existed
PromotionTypeModel.sync({
  create: true
}).then(function () {
  _initData2.default.promotionTypeList.map(function (e) {
    PromotionTypeModel.findOrCreate({
      where: {
        type_id: e.type_id
      },
      defaults: _extends({}, e)
    });
  });
});

exports.default = PromotionTypeModel;
//# sourceMappingURL=promotion_type.model.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _services = require('../services');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
var jsonParser = _bodyParser2.default.json();

router.get('/cart/products', function (req, res) {
  // TODO get cart lists
  return res.json({ 'TODO': 'get cart lists' });
});

router.post('/cart/_checkout', jsonParser, function (req, res) {
  // return res.json(req.body);
  _services.checkoutService.checkout(req.body).then(function (data) {
    return res.json(data);
  });
});

exports.default = router;
//# sourceMappingURL=cartRouter.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _productRouter = require('./productRouter');

var _productRouter2 = _interopRequireDefault(_productRouter);

var _cartRouter = require('./cartRouter');

var _cartRouter2 = _interopRequireDefault(_cartRouter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  productRouter: _productRouter2.default,
  cartRouter: _cartRouter2.default
};
//# sourceMappingURL=index.js.map
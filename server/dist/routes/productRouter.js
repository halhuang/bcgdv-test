'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _services = require('../services');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
var jsonParser = _bodyParser2.default.json();

router.get('/products', function (req, res) {
  _services.productService.getProductList().then(function (data) {
    return res.json(data);
  });
});

router.post('/products/_add', jsonParser, function (req, res) {
  _services.productService.addProduct(req.body).then(function (data) {
    return res.json(data);
  });
});

exports.default = router;
//# sourceMappingURL=productRouter.js.map
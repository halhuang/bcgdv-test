'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/**
 * type: 0001
 *   buy one with one another free, with 3 variables
 *   checkAnotherFree: originAmount, newProductsSku, freeAmount
 * type: 0002
 *   buy more with less amount price, with 2 variables
 *   checkLessAmount: originAmount, priceAmount
 * type: 0003
 *   buy more with discount, with 2 variables
 *   checkDiscount: amount, rate
 */
/**
 * output: {
 *   sku: string,
 *   product: object,
 *   amount: number,
 *   price: number,
 *   originTotalPrice: number,
 *   promotionPrice: number,
 *   totalPrice: number,
 *   promotionType: string,
 *   promotionName: string,
 *   promotionProduct: object
 * }
 */

var filterPromotion = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(item, promotionListItem, product) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(promotionListItem.type_id === '0001')) {
              _context.next = 6;
              break;
            }

            _context.next = 3;
            return checkAnotherFree(item, promotionListItem, product);

          case 3:
            return _context.abrupt('return', _context.sent);

          case 6:
            if (!(promotionListItem.type_id === '0002')) {
              _context.next = 10;
              break;
            }

            return _context.abrupt('return', checkLessAmount(item, promotionListItem, product));

          case 10:
            if (!(promotionListItem.type_id === '0003')) {
              _context.next = 14;
              break;
            }

            return _context.abrupt('return', checkDiscount(item, promotionListItem, product));

          case 14:
            return _context.abrupt('return', checkWithNoPromotion(item));

          case 15:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function filterPromotion(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

var checkAnotherFree = function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(item, promotionListItem, product) {
    var promotionProduct;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return (0, _common.getProductDetail)(promotionListItem.variable2);

          case 2:
            promotionProduct = _context2.sent;

            if (!(Math.floor(item.amount / promotionListItem.variable1) > 0)) {
              _context2.next = 7;
              break;
            }

            return _context2.abrupt('return', {
              sku: item.sku,
              product: _extends({}, product),
              amount: item.amount,
              price: product.price,
              originTotalPrice: product.price * item.amount,
              promotionPrice: 0,
              totalPrice: product.price * item.amount,
              promotionType: promotionListItem.type_id,
              promotionName: promotionListItem.promotion_type.name,
              promotionProduct: {
                product: _extends({}, promotionProduct),
                amount: Math.floor(item.amount / promotionListItem.variable1) * promotionListItem.variable3
              }
            });

          case 7:
            return _context2.abrupt('return', checkWithNoPromotion(item, product));

          case 8:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function checkAnotherFree(_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}();

var _find = require('lodash/find');

var _find2 = _interopRequireDefault(_find);

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

var _models = require('../models');

var _common = require('./common');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function checkLessAmount(item, promotionListItem, product) {
  var mainCountTime = Math.floor(item.amount / +promotionListItem.variable1);
  var noPromotionCount = item.amount % +promotionListItem.variable1;

  if (mainCountTime > 0) {
    return {
      sku: item.sku,
      product: _extends({}, product),
      amount: item.amount,
      price: product.price,
      originTotalPrice: product.price * item.amount,
      promotionPrice: -(product.price * (+promotionListItem.variable1 - +promotionListItem.variable2) * mainCountTime),
      totalPrice: product.price * (mainCountTime * +promotionListItem.variable2 + noPromotionCount),
      promotionType: promotionListItem.type_id,
      promotionName: promotionListItem.promotion_type.name,
      promotionProduct: {}
    };
  } else {
    return checkWithNoPromotion(item, product);
  }
}

function checkDiscount(item, promotionListItem, product) {
  if (item.amount >= +promotionListItem.variable1) {
    return {
      sku: item.sku,
      product: _extends({}, product),
      amount: item.amount,
      price: product.price,
      originTotalPrice: product.price * item.amount,
      promotionPrice: -product.price * item.amount * (1 - promotionListItem.variable2),
      totalPrice: product.price * item.amount * +promotionListItem.variable2,
      promotionType: promotionListItem.type_id,
      promotionName: promotionListItem.promotion_type.name,
      promotionProduct: {}
    };
  } else {
    return checkWithNoPromotion(item, product);
  }
}

function checkWithNoPromotion(item, product) {
  return {
    sku: item.sku,
    product: _extends({}, product),
    amount: item.amount,
    price: product.price,
    originTotalPrice: product.price * item.amount,
    promotionPrice: 0,
    totalPrice: product.price * item.amount,
    promotionType: '',
    promotionName: '',
    promotionProduct: {}
  };
}

/**
 * check if in Promotion
 * @param  {[]}  promotionList
 * @param  {object}  item
 * @return {object || underfinedl}               if found nothing return underfined
 */
function isInPromotion(promotionList, item) {
  return (0, _find2.default)(promotionList, { product_sku: item.sku });
}

/**
 * final check for free item
 * if the item is free, remove it from list and change the statistical prices
 * @param  {obj} origin obj
 * @return {obj} filterd 
 */
function filterForFree(origin) {
  var rs = {
    list: [],
    originTotalPrice: origin.originTotalPrice,
    promotionPrice: origin.promotionPrice,
    totalPrice: origin.totalPrice
  };
  var freeItemsList = [];
  origin.list.map(function (e) {
    if (!(0, _isEmpty2.default)(e.promotionProduct)) {
      freeItemsList.push({
        sku: e.promotionProduct.product.sku,
        amount: e.promotionProduct.amount
      });
    }
  });

  origin.list.map(function (e) {
    if (!(0, _isEmpty2.default)(freeItemsList.filter(function (f) {
      return f.sku === e.sku;
    }))) {
      var freeItem = freeItemsList.filter(function (f) {
        return f.sku === e.sku;
      })[0];
      if (e.amount <= freeItem.amount) {
        rs.promotionPrice -= e.product.price * e.amount;
        rs.totalPrice -= e.product.price * e.amount;
      } else {
        var diff = e.amount - freeItem.amount;
        rs.promotionPrice -= e.product.price * freeItem.amount;
        rs.totalPrice -= e.product.price * freeItem.amount;
        e.amount = diff;
        rs.list.push(e);
      }
    } else {
      rs.list.push(e);
    }
  });

  return rs;
}

exports.default = {
  checkout: function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(data) {
      var rs, promotionList, i, product, promotionListItem, tmpData, promotionItem;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              rs = {
                list: [],
                originTotalPrice: 0,
                promotionPrice: 0,
                totalPrice: 0
              };
              _context3.next = 3;
              return (0, _common.getPromotionList)();

            case 3:
              promotionList = _context3.sent;
              i = 0;

            case 5:
              if (!(i < data.length)) {
                _context3.next = 26;
                break;
              }

              _context3.next = 8;
              return (0, _common.getProductDetail)(data[i].sku);

            case 8:
              product = _context3.sent;
              promotionListItem = isInPromotion(promotionList, data[i]);
              tmpData = {};

              if (!promotionListItem) {
                _context3.next = 18;
                break;
              }

              _context3.next = 14;
              return filterPromotion(data[i], promotionListItem, product);

            case 14:
              promotionItem = _context3.sent;

              tmpData = promotionItem;
              _context3.next = 19;
              break;

            case 18:
              tmpData = checkWithNoPromotion(data[i], product);

            case 19:

              rs.list.push(tmpData);
              rs.originTotalPrice += tmpData.originTotalPrice;
              rs.promotionPrice += tmpData.promotionPrice;
              rs.totalPrice += tmpData.totalPrice;

            case 23:
              i++;
              _context3.next = 5;
              break;

            case 26:

              rs = filterForFree(rs);
              rs.list.map(function (e) {
                e.promotionPrice = e.promotionPrice.toFixed(2);
                e.totalPrice = e.totalPrice.toFixed(2);
              });

              return _context3.abrupt('return', Promise.resolve({
                list: [].concat(_toConsumableArray(rs.list)),
                originTotalPrice: rs.originTotalPrice.toFixed(2),
                promotionPrice: rs.promotionPrice.toFixed(2),
                totalPrice: rs.totalPrice.toFixed(2)
              }));

            case 29:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined);
    }));

    return function checkout(_x7) {
      return _ref3.apply(this, arguments);
    };
  }()
};
//# sourceMappingURL=checkoutService.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPromotionList = exports.getProductDetail = undefined;

/**
 * get Product Detail Object from database
 * @param  {string} sku
 * @return {object}     
 */
var getProductDetail = exports.getProductDetail = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(sku) {
    var rs, data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            rs = {};
            _context.next = 3;
            return _models.productModel.findOne({
              where: {
                sku: sku
              }
            }).then(function (data) {
              rs = data.get();
            });

          case 3:
            data = _context.sent;
            return _context.abrupt('return', rs);

          case 5:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function getProductDetail(_x) {
    return _ref.apply(this, arguments);
  };
}();

// TODO Store in Redis and Get from Redis
/**
 * get Promotion List
 * @return {[]object}
 */


var getPromotionList = exports.getPromotionList = function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var rs, datas;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            rs = [];
            _context2.next = 3;
            return _models.PromotionListModel.findAll({
              include: [{
                model: _models.PromotionTypeModel,
                as: 'promotion_type'
              }]
            }).then(function (data) {
              data.map(function (e) {
                rs.push(e.get());
              });
            });

          case 3:
            datas = _context2.sent;
            return _context2.abrupt('return', rs);

          case 5:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function getPromotionList() {
    return _ref2.apply(this, arguments);
  };
}();

var _models = require('../models');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }
//# sourceMappingURL=common.js.map
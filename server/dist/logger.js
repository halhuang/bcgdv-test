'use strict';

/* eslint-disable no-console */

var chalk = require('chalk');
var ip = require('ip');

var divider = chalk.gray('\n-----------------------------------');

/**
 * Logger middleware, you can customize it to make messages more personal
 */
var logger = {
  // Called whenever there's an error on the server we want to print
  error: function error(err) {
    console.error(chalk.red(err));
  },

  // Called when express.js app starts on given port w/o errors
  appStarted: function appStarted(port, host, tunnelStarted) {
    console.log('Server started ! ' + chalk.green('✓'));

    // If the tunnel started, log that and the URL it's available at
    if (tunnelStarted) {
      console.log('Tunnel initialised ' + chalk.green('✓'));
    }

    console.log('\n' + chalk.bold('Access URLs:') + divider + '\nLocalhost: ' + chalk.magenta('http://' + host + ':' + port) + '\n      LAN: ' + (chalk.magenta('http://' + ip.address() + ':' + port) + (tunnelStarted ? '\n    Proxy: ' + chalk.magenta(tunnelStarted) : '')) + divider + '\n' + chalk.blue('Press ' + chalk.italic('CTRL-C') + ' to stop') + '\n    ');
  }
};

module.exports = logger;
//# sourceMappingURL=logger.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DB = exports.System = undefined;

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// system config
var System = exports.System = {
  db_type: 'mysql', // db Type
  bodyLimit: '1024kb',
  port: 3001,
  host: 'localhost'

  // db config
};var DB = exports.DB = {
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'mousic',
  database: 'bcgdv_test',
  prefix: 'api_'
};
//# sourceMappingURL=config.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _apicache = require('apicache');

var _apicache2 = _interopRequireDefault(_apicache);

var _config = require('./config');

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

require('regenerator-runtime/runtime');

require('./lib/sequelize');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var port = process.env.PORT || _config.System.port;
var isDev = process.env.NODE_ENV !== 'production';

var app = (0, _express2.default)();
app.server = _http2.default.createServer(app);

app.use((0, _cors2.default)());
app.use(_bodyParser2.default.json({
  limit: _config.System.bodyLimit
}));

var cache = _apicache2.default.middleware;

// cache products api in 1 minutes
// app.use('/api/v1', cache('1 minutes'), Router.productRouter);
app.use(_express2.default.static(_path2.default.join(__dirname, '../../client/dist/')));
app.use('/api/v1', _routes2.default.productRouter);
app.use('/api/v1', _routes2.default.cartRouter);
// ***
app.use(function (req, res) {
  res.sendFile('index.html', { root: _path2.default.join(__dirname, '../../client/dist/') });
});

app.listen(port, _config.System.host, function (err) {
  console.log('Started on port ' + port);
  if (err) {
    return _logger2.default.error(err.message);
  }
  _logger2.default.appStarted(port, _config.System.host);
});

exports.default = app;
//# sourceMappingURL=index.js.map
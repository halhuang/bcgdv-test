'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.db = exports.Sequelize = undefined;

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var db = new _sequelize2.default(_config.DB.database, _config.DB.username, _config.DB.password, {
  host: _config.DB.host,
  dialect: _config.System.db_type,
  define: {
    underscored: true
  },
  dialectOptions: { // MySQL > 5.5
    charset: 'utf8',
    collate: 'utf8_general_ci',
    supportBigNumbers: true,
    bigNumberStrings: true
  },
  pool: {
    // max: 50, // default
    max: 1, // for test
    min: 0,
    idle: 10000
  }
});

db.authenticate().then(function () {
  console.log('Connect Mysql correct!');
}).catch(function (err) {
  console.error('Connect Mysql Error:', err);
});

exports.Sequelize = _sequelize2.default;
exports.db = db;
//# sourceMappingURL=sequelize.js.map
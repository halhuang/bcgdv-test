import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import path from 'path';
import cors from 'cors';
import apicache from 'apicache';

import { System as SystemConfig } from './config';
import Router from './routes';
import logger from './logger';

import "regenerator-runtime/runtime";
import './lib/sequelize';

const port = process.env.PORT || SystemConfig.port;
const isDev = process.env.NODE_ENV !== 'production';

let app = express();
app.server = http.createServer(app);


const errorHandler = (err, req, res, next) => {
  if (res.headersSent) {
    return next(err)
  }
  res.status(500)
  res.render('error', { error: err })
}

app.use(cors());
app.use(bodyParser.json({
  limit: SystemConfig.bodyLimit
}));

let cache = apicache.middleware;

// cache products api in 1 minutes
// app.use('/api/v1', cache('1 minutes'), Router.productRouter);
app.use(express.static(path.join(__dirname, '../../client/dist/')));
app.use('/api/v1', Router.productRouter);
app.use('/api/v1', Router.cartRouter);
app.use(errorHandler);

// ***
app.use('*', (req, res) => {
  res.sendFile('index.html', { root: path.join(__dirname, '../../client/dist/')});
});


app.listen(port, SystemConfig.host,  err => {
  console.log(`Started on port ${port}`);
  if (err) {
    return logger.error(err.message);
  }
  logger.appStarted(port, SystemConfig.host);
});


export default app;

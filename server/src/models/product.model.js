import { DB as config } from '../config';
import { Sequelize, db } from '../lib/sequelize';
import initData from '../initData';

const ProductModel = db.define(`${config.prefix}products`, {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  sku: {
    type: Sequelize.STRING(6),
  },
  name: {
    type: Sequelize.STRING(256),
  },
  price: {
    type: Sequelize.FLOAT(16),
  },
  currency: {
    type: Sequelize.STRING(8),
    defaultValue: 'USD',
  },
  inventory_qty: {
    type: Sequelize.INTEGER,
    defaultValue: 0,
  },
  state: {
    type: Sequelize.INTEGER,
    defaultValue: 1, // -1 del, 0 offline, 1 online
  },
  promotion: {
    type: Sequelize.STRING(32)
  },
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
},{
  tableName: `${config.prefix}products`,
  comment: 'products list',
});

// create table if not existed
ProductModel.sync({
  create: true,
}).then(() => {
  initData.productList.map(e => {
    ProductModel.findOrCreate({
      where: {
        sku: e.sku
      },
      defaults: {
        ...e,
      }
    })
  });
});

export default ProductModel;

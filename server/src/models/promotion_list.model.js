import { DB as config } from '../config';
import { Sequelize, db } from '../lib/sequelize';
import initData from '../initData';
import PromotionTypeModel from './promotion_type.model';

const PromotionListModel = db.define(`${config.prefix}promotion_list`, {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  uuid: {
    type: Sequelize.STRING(36),
  },
  type_id: {
    type: Sequelize.STRING(4),
    unique: true,
  },
  product_sku: {
    type: Sequelize.STRING(6),
  },
  variable1: {
    type: Sequelize.STRING(32),
  },
  variable2: {
    type: Sequelize.STRING(32),
  },
  variable3: {
    type: Sequelize.STRING(32),
  },
  variable4: {
    type: Sequelize.STRING(32),
  },
  start_time: {
    type: Sequelize.DATE,
  },
  end_time: {
    type: Sequelize.DATE,
  },
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
},{
  tableName: `${config.prefix}promotion_list`,
  comment: 'promotion list',
})

// create table if not existed
PromotionListModel.sync({
  create: true,
}).then(() => {
  initData.promotionList.map(e => {
    PromotionListModel.findOrCreate({
      where: {
        uuid: e.uuid,
      },
      defaults: {
        ...e,
      }
    });
  });
  PromotionListModel.belongsTo(PromotionTypeModel, {foreignKey: 'type_id', as: 'promotion_type'});
});

export default PromotionListModel;

import { DB as config } from '../config';
import { Sequelize, db } from '../lib/sequelize';
import initData from '../initData';

const PromotionTypeModel = db.define(`${config.prefix}promotion_type`, {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  type_id: {
    type: Sequelize.STRING(4),
    unique: true,
  },
  name: {
    type: Sequelize.STRING(256),
  },
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
},{
  tableName: `${config.prefix}promotion_type`,
  comment: 'promotion type',
});

// create table if not existed
PromotionTypeModel.sync({
  create: true,
}).then(() => {
  initData.promotionTypeList.map(e => {
    PromotionTypeModel.findOrCreate({
      where: {
        type_id: e.type_id,
      },
      defaults: {
        ...e,
      }
    })
  })
});



export default PromotionTypeModel;

import productModel from './product.model';
import PromotionTypeModel from './promotion_type.model';
import PromotionListModel from './promotion_list.model';

export {
  productModel,
  PromotionTypeModel,
  PromotionListModel,
}
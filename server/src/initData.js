// import uuidv4 from 'uuid/v4';
// import { productModel, PromotionTypeModel, PromotionListModel } from './models';

// generate random uuid
// const testId = uuidv4(); // 10ba038e-48da-487b-96e8-8d3b99b6d18a

// product
const productList = [
  {
    sku: '120P90',
    name: 'Google Home',
    price: 49.99,
    currency: 'USD',
    inventory_qty: 10,
  },{
    sku: '43N23P',
    name: 'MacBook Pro',
    price: 5399.99,
    currency: 'USD',
    inventory_qty: 5,
  },{
    sku: 'A304SD',
    name: 'Alexa Speaker',
    price: 109.50,
    currency: 'USD',
    inventory_qty: 10,
  },{
    sku: '234234',
    name: 'Raspberry Pi B',
    price: 30.00,
    currency: 'USD',
    inventory_qty: 2,
  }
];

// promotion_type
const promotionTypeList = [
  {
    type_id: '0000',
    name: 'with free',
  },{
    type_id: '0001',
    name: 'more with less'
  },{
    type_id: '0002',
    name: 'discount'
  }
];

// promotion_list
const promotionList = [
  {
    uuid: 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa',
    type_id: '0001',
    product_sku: '43N23P',
    variable1: '1',
    variable2: '234234',
    variable3: '1',
    start_time: '2019-01-01',
    end_time: '2020-01-01',
  },{
    uuid: 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb',
    type_id: '0002',
    product_sku: '120P90',
    variable1: '3',
    variable2: '2',
    start_time: '2019-01-01',
    end_time: '2020-01-01',
  },{
    uuid: 'cccccccc-cccc-cccc-cccc-cccccccccccc',
    type_id: '0003',
    product_sku: 'A304SD',
    variable1: '3',
    variable2: '0.9',
    start_time: '2019-01-01',
    end_time: '2020-01-01',
  }
];

export default {
  productList,
  promotionTypeList,
  promotionList,
}

//init script

// productList.map(e => {
//   productModel.findOrCreate({
//     where: {
//       sku: e.sku
//     },
//     defaults: {
//       ...e,
//     }
//   })
// });

// promotionTypeList.map(e => {
//   PromotionTypeModel.findOrCreate({
//     where: {
//       type_id: e.type_id,
//     },
//     defaults: {
//       ...e,
//     }
//   })
// });

// promotionList.map(e => {
//   PromotionListModel.findOrCreate({
//     where: {
//       uuid: e.uuid,
//     },
//     defaults: {
//       ...e,
//     }
//   });
// })

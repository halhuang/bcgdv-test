import path from 'path';

// system config
export const System = {
  db_type: 'mysql', // db Type
  bodyLimit: '1024kb',
  port: 3001,
  host: 'localhost',
}

// db config
export const DB = {
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'mousic',
  database: 'bcgdv_test',
  prefix: 'api_'
}

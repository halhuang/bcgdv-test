import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import { PromotionTypeModel, PromotionListModel } from '../models';
import { getProductDetail, getPromotionList } from './common';

/**
 * type: 0001
 *   buy one with one another free, with 3 variables
 *   checkAnotherFree: originAmount, newProductsSku, freeAmount
 * type: 0002
 *   buy more with less amount price, with 2 variables
 *   checkLessAmount: originAmount, priceAmount
 * type: 0003
 *   buy more with discount, with 2 variables
 *   checkDiscount: amount, rate
 */
/**
 * output: {
 *   sku: string,
 *   product: object,
 *   amount: number,
 *   price: number,
 *   originTotalPrice: number,
 *   promotionPrice: number,
 *   totalPrice: number,
 *   promotionType: string,
 *   promotionName: string,
 *   promotionProduct: object
 * }
 */

async function filterPromotion(item, promotionListItem, product) {
  if(promotionListItem.type_id === '0001') {
    return await checkAnotherFree(item, promotionListItem, product);
  } else if(promotionListItem.type_id === '0002') {
    return checkLessAmount(item, promotionListItem, product);
  } else if(promotionListItem.type_id === '0003') {
    return checkDiscount(item, promotionListItem, product);
  } else {
    return checkWithNoPromotion(item);
  }
}

async function checkAnotherFree(item, promotionListItem, product) {
  const promotionProduct = await getProductDetail(promotionListItem.variable2);
  if(Math.floor(item.amount/promotionListItem.variable1) > 0) {
    return {
      sku: item.sku,
      product: {...product},
      amount: item.amount,
      price: product.price,
      originTotalPrice: product.price * item.amount,
      promotionPrice: 0,
      totalPrice: product.price * item.amount,
      promotionType: promotionListItem.type_id,
      promotionName: promotionListItem.promotion_type.name,
      promotionProduct: {
        product: {...promotionProduct},
        amount: Math.floor(item.amount/promotionListItem.variable1) * promotionListItem.variable3,
      }
    }
  } else {
    return checkWithNoPromotion(item, product);
  }
    
}

function checkLessAmount(item, promotionListItem, product) {
  const mainCountTime = Math.floor(item.amount / +promotionListItem.variable1);
  const noPromotionCount = item.amount % +promotionListItem.variable1;

  if(mainCountTime > 0) {
    return {
      sku: item.sku,
      product: {...product},
      amount: item.amount,
      price: product.price,
      originTotalPrice: product.price * item.amount,
      promotionPrice: - (product.price * ( +promotionListItem.variable1 - +promotionListItem.variable2) * mainCountTime),
      totalPrice: product.price * ((mainCountTime * +promotionListItem.variable2) + noPromotionCount),
      promotionType: promotionListItem.type_id,
      promotionName: promotionListItem.promotion_type.name,
      promotionProduct: {}
    }
  } else {
    return checkWithNoPromotion(item, product);
  }
}

function checkDiscount(item, promotionListItem, product) {
  if(item.amount >= +promotionListItem.variable1) {
    return {
      sku: item.sku,
      product: {...product},
      amount: item.amount,
      price: product.price,
      originTotalPrice: product.price * item.amount,
      promotionPrice: - product.price * item.amount * (1 - promotionListItem.variable2),
      totalPrice: product.price * item.amount * (+promotionListItem.variable2),
      promotionType: promotionListItem.type_id,
      promotionName: promotionListItem.promotion_type.name,
      promotionProduct: {}
    }
  } else {
    return checkWithNoPromotion(item, product);
  }
}

function checkWithNoPromotion(item, product) {
  return {
    sku: item.sku,
    product: {...product},
    amount: item.amount,
    price: product.price,
    originTotalPrice: product.price * item.amount,
    promotionPrice: 0,
    totalPrice: product.price * item.amount,
    promotionType: '',
    promotionName: '',
    promotionProduct: {},
  }
}


/**
 * check if in Promotion
 * @param  {[]}  promotionList
 * @param  {object}  item
 * @return {object || underfinedl}               if found nothing return underfined
 */
function isInPromotion(promotionList, item) {
  return find(promotionList, {product_sku: item.sku});
}


/**
 * final check for free item
 * if the item is free, remove it from list and change the statistical prices
 * @param  {obj} origin obj
 * @return {obj} filterd 
 */
function filterForFree(origin) {
  let rs = {
    list: [],
    originTotalPrice: origin.originTotalPrice,
    promotionPrice: origin.promotionPrice,
    totalPrice: origin.totalPrice,
  };
  const freeItemsList = [];
  origin.list.map(e => {
    if(!isEmpty(e.promotionProduct)) {
      freeItemsList.push({
        sku: e.promotionProduct.product.sku,
        amount: e.promotionProduct.amount,
      })
    }
  });

  origin.list.map(e => {
    if(!isEmpty(freeItemsList.filter(f => f.sku === e.sku))) {
      const freeItem = freeItemsList.filter(f => f.sku === e.sku)[0];
      if(e.amount <= freeItem.amount) {
        rs.promotionPrice -= e.product.price * e.amount;
        rs.totalPrice -= e.product.price * e.amount;
      } else {
        const diff = e.amount - freeItem.amount;
        rs.promotionPrice -= e.product.price * freeItem.amount;
        rs.totalPrice -= e.product.price * freeItem.amount;
        e.amount = diff;
        rs.list.push(e);
      }
    } else {
      rs.list.push(e);
    }
  })

  return rs;
}

export default {
  checkout: async (data) => {
    let rs = {
      list: [],
      originTotalPrice: 0,
      promotionPrice: 0,
      totalPrice: 0,
    };
    const promotionList = await getPromotionList();
    
    // check each item with promotion
    for (let i = 0; i < data.length; i++) {
      const product = await getProductDetail(data[i].sku);
      const promotionListItem = isInPromotion(promotionList, data[i]);

      let tmpData = {};
      if(promotionListItem) {
        const promotionItem = await filterPromotion(data[i], promotionListItem, product)
        tmpData = promotionItem;
      } else {
        tmpData = checkWithNoPromotion(data[i], product);
      }

      rs.list.push(tmpData);
      rs.originTotalPrice += tmpData.originTotalPrice;
      rs.promotionPrice += tmpData.promotionPrice;
      rs.totalPrice += tmpData.totalPrice;
    }

    rs = filterForFree(rs);
    rs.list.map(e => {
      e.promotionPrice = e.promotionPrice.toFixed(2);
      e.totalPrice = e.totalPrice.toFixed(2);
    });    

    return Promise.resolve({
      list: [...rs.list],
      originTotalPrice: rs.originTotalPrice.toFixed(2),
      promotionPrice: rs.promotionPrice.toFixed(2),
      totalPrice: rs.totalPrice.toFixed(2),
    });
  },
}
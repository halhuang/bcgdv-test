import { productModel } from '../models';
import "regenerator-runtime/runtime";

export default {
  getProductList: () => {
    return productModel.findAll({}, (err, datas) => {
      if(err) {
        throw err;
      } else {
        return datas;
      }
    })
  },
  addProduct: async (newProduct) => {
    return productModel.findOrCreate({where: {sku: newProduct.sku}, defaults: {...newProduct}})
    .then(([data, created]) => {
      if(created) {
        return data.get({
          plain: true
        });
      } else {
        return {};
      }
    })
  }
}
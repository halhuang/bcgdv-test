import { productModel, PromotionListModel, PromotionTypeModel } from '../models';

/**
 * get Product Detail Object from database
 * @param  {string} sku
 * @return {object}     
 */
export async function getProductDetail(sku) {
  let rs = {};
  const data = await productModel.findOne({
    where: {
      sku: sku,
    }
  }).then(data => {
    rs = data.get();
  });

  return rs;
}

// TODO Store in Redis and Get from Redis
/**
 * get Promotion List
 * @return {[]object}
 */
export async function getPromotionList() {
  let rs = [];
  const datas = await PromotionListModel.findAll({
    include: [{
      model: PromotionTypeModel,
      as: 'promotion_type',
    }],
  }).then(data => {
    data.map(e => {
      rs.push(e.get());
    });
  });

  return rs;
}
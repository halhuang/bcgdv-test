import productRouter from './productRouter';
import cartRouter from './cartRouter';

export default {
  productRouter,
  cartRouter,
};
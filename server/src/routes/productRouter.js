import express from 'express';
import bodyParser from 'body-parser';
import { productService } from '../services';

const router = express.Router();
const jsonParser = bodyParser.json();

router.get('/products', (req, res, next) => {
  productService.getProductList()
    .then(data => {
      return res.json(data);
    })
    .catch(err => {
      next(err);
    });
});

router.post('/products/_add', jsonParser, (req, res, next) => {
  productService.addProduct(req.body)
    .then(data => {
      return res.json(data);
    })
    .catch(err => {
      next(err);
    })
})

export default router;
import express from 'express';
import bodyParser from 'body-parser';
import { checkoutService } from '../services';

const router = express.Router();
const jsonParser = bodyParser.json();

router.get('/cart/products', (req, res, next) => {
  // TODO get cart lists
  return res.json({'TODO': 'get cart lists'});
});

router.post('/cart/_checkout', jsonParser, (req, res, next) => {
  // return res.json(req.body);
  checkoutService.checkout(req.body)
    .then(data => {
      return res.json(data);
    })
    .catch(err => {
      next(err);
    })
})

export default router;
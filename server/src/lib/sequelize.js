import Sequelize from 'sequelize';
import { DB as DBConfig, System as SystemConfig } from '../config';

const db = new Sequelize(DBConfig.database, DBConfig.username, DBConfig.password, {
  host: DBConfig.host,
  dialect: SystemConfig.db_type,
  define: {
    underscored: true
  },
  dialectOptions: { // MySQL > 5.5
    charset: 'utf8',
    collate: 'utf8_general_ci',
    supportBigNumbers: true,
    bigNumberStrings: true
  },
  pool: {
    // max: 50, // default
    max: 1, // for test
    min: 0,
    idle: 10000
  }
});

db.authenticate()
  .then(() => {
    console.log('Connect Mysql correct!')
  })
  .catch(err => {
    console.error('Connect Mysql Error:', err)
  });

export { Sequelize, db }

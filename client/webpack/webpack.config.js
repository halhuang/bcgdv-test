var devConfig = require('./webpack.dev.config.js');
var prodConfig = require('./webpack.prod.config.js');

module.exports = (env) => {
  if (env && env.NODE_ENV === 'production') {
    return prodConfig;
  }
  return devConfig;
};

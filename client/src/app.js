import React from 'react';
import ReactDom from 'react-dom';
import isEmpty from 'lodash/isEmpty';

import { getProducts, checkOut } from './utils/api';
import './app.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productsList: [],
      result: {
        list: [],
        originTotalPrice: 0,
        promotionPrice: 0,
        totalPrice: 0,
      }
    };

    this.checkOut = this.checkOut.bind(this);
  }

  componentDidMount() {
    getProducts().then(data => {
      data.map(e => {
        e.amount = 0;
      })
      this.setState({
        productsList: data || [],
      })
    });
  }

  changeAmount(sku, value) {
    const data = this.state.productsList;
    data.map(e => {
      if(e.sku === sku) {
        e.amount = value < 0 ? 0 : value;
      }
    });
    this.setState({
      productsList: data,
    })
  }

  checkOut() {
    const data = [];
    this.state.productsList.map(e => {
      if(e.amount > 0) {
        data.push({
          sku: e.sku,
          amount: e.amount,
        })
      }
    })
    console.log(data);
    checkOut(data).then(data => {
      this.setState({
        result: data,
      })
    });
  }

  render() {
    return (
      <div className='container'>
        <table>
          <thead>
            <tr>
              <th>sku</th>
              <th>name</th>
              <th>price</th>
              <th>amount</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
            {this.state.productsList.map(e => (
              <tr key={e.sku}>
                <td>{e.sku}</td>
                <td>{e.name}</td>
                <td>${(+e.price).toFixed(2)}</td>
                <td>{e.amount}</td>
                <td>
                  <button onClick={() => this.changeAmount(e.sku, e.amount - 1)}>-</button>
                  <button onClick={() => this.changeAmount(e.sku, e.amount + 1)}>+</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <hr/>
        <button className='check-btn' onClick={this.checkOut}>Check</button>
        {this.state.result.list.length > 0 && (
          <table>
            <thead>
              <tr>
                <td>sku</td>
                <td>name</td>
                <td>price</td>
                <td>amount</td>
                <td>promotion name</td>
                <td>promotion Price</td>
                <td>Total Price</td>
              </tr>
            </thead>
            <tbody>
              {this.state.result.list.map(e => (
                <tr key={e.sku}>
                  <td>{e.sku}</td>
                  <td>{e.product.name}</td>
                  <td>${e.price}</td>
                  <td>{e.amount}</td>
                  <td>{e.promotionName || '-'}</td>
                  <td>{e.promotionPrice < 0 ? `-$${-e.promotionPrice}` : '-'}</td>
                  <td>${e.totalPrice}</td>
                </tr>
              ))}
              <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              {this.state.result.list.filter(e => !isEmpty(e.promotionProduct)).map(e => (
                <tr key={e.promotionProduct.product.sku}>
                  <td>gift</td>
                  <td>***</td>
                  <td>{e.promotionProduct.product.sku}</td>
                  <td>{e.promotionProduct.product.name}</td>
                  <td>*{e.promotionProduct.amount}</td>
                  <td>***</td>
                  <td>gift</td>
                </tr>
              ))}
              <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>total Promotion</td>
                <td>{this.state.result.promotionPrice < 0 ? `-$${-this.state.result.promotionPrice}` : '-'}</td>
                <td>total Price</td>
                <td>{this.state.result.totalPrice ? `$${this.state.result.totalPrice}` : '-'}</td>
              </tr>
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

ReactDom.render(
  <App />,
  document.getElementById('app'),
);

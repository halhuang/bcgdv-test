import config from '../config';
import request from './request';

export function getProducts() {
  return request.get('products');
}

export function checkOut(data) {
  return request.post('cart/_checkout', data);
}

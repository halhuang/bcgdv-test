import axios from 'axios';
import config from '../config';
import "regenerator-runtime/runtime";

axios.create({
  timeout: 5000 ,
  headers: {
    'Content-Type': 'application/json'
  },
});

axios.interceptors.response.use(
  response => {
    const res = response.data;
 
    // TODO api structure
    // if (res.state === "success") {
    //   return response.data;
    // } else {
    //   return Promise.reject(error);
    // }
    return Promise.resolve(response.data);
  },
  error => {
    return Promise.reject(error);
  }
);

export default {
  get: (url, data={}) => {
    return axios({
      url: config.apiURL + url,
      method: 'get',
      contentType: "application/json",
    });
  },
  post: (url, data={}) => {
    return axios({
      url: config.apiURL + url,
      method: 'post',
      contentType: "application/json",
      data: data,
    });
  }
}

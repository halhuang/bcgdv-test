# BCGDV Test

## dev server

the environment shoul have mysql, and have a created database.

### api-server

change the config under `./server/src/config.js`,

run:

```bash
cd ./server
yarn
yarn dev
```

the server will start at http://127.0.0.1:3001
the client will start at http://127.0.0.1:8080


### client

run:

```bash
cd ./client
yarn
yarn dev
```

## production

client

```bash
cd client
yarn
yarn build
```

server
```bash
cd server
yarn
yarn build
```

deployment
```bash
pm2 install pm2-logrotate
pm2 start ecosystem.config.js
```

the project will start at http://127.0.0.1:3001
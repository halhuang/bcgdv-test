module.exports = {
  apps : [{
    name: 'server',
    script: './server/dist/index.js',

    // log
    output: './server/log/output/out.log',
    error: './server/log/error/error.log',
    log: './server/log/combined/combined.outer.log',
    merge_logs: true,

    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    },
  }],
};
